#ifndef __XMAS_SCANNER_H_
#define __XMAS_SCANNER_H_

#include "scanner.h"

class XmasScanner : public Scanner {
  public:
    XmasScanner(std::string host, std::string source_address);

    /**
     * Half open tcp scanning.
     */
    PortStatus scanPort(std::string port);

  private:
    std::string source_address;
};

#endif // __XMAS_SCANNER_H_
