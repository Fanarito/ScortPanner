#ifndef __TCP_SCANNER_H_
#define __TCP_SCANNER_H_

#include "netinet/tcp.h"
#include "scanner.h"

class TcpScanner : public Scanner {
  public:
    using Scanner::Scanner;

    /**
     * Tries to establish a full tcp connection with the host at the given port number.
     * If it is able to establish a connection with at most 2 retries means that the
     * port is open. Otherwise assume it is closed.
     */
    PortStatus scanPort(std::string port);
};

#endif // __TCP_SCANNER_H_
