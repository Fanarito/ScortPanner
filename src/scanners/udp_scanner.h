#ifndef __UDP_SCANNER_H_
#define __UDP_SCANNER_H_

#include "scanner.h"

class UdpScanner : public Scanner {
  public:
    using Scanner::Scanner;

    /**
     * Sends an udp packet, if there is no response we assume either it is open or filtered. If
     * there is a ICMP response it is closed.
     */
    PortStatus scanPort(std::string port);
};

#endif // __UDP_SCANNER_H_
