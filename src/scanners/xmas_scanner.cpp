#include "xmas_scanner.h"

XmasScanner::XmasScanner(std::string host, std::string source_address)
    : Scanner(host), source_address(source_address) {}

PortStatus XmasScanner::scanPort(std::string port) {
    addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_protocol = IPPROTO_TCP;
    addrinfo *server_info = getAddressInfo(port.data(), &hints);
    if (server_info == NULL) {
        return PortStatus::Closed;
    }

    // Setup write socket
    int write_socket = createSocket(PF_INET, SOCK_RAW, IPPROTO_TCP);
    // Setup read socket
    int read_socket = createSocket(PF_INET, SOCK_RAW, IPPROTO_TCP);

    timeval time_val;
    time_val.tv_sec = 5;
    time_val.tv_usec = 0;

    if (setsockopt(read_socket, SOL_SOCKET, SO_RCVTIMEO, &time_val, sizeof(struct timeval)) <
        0) {
        perror("Setting read socket timeout");
    }
    int opt = 1;
    if (setsockopt(read_socket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) < 0) {
        perror("Setting read socket REUSEADDR");
    }

    // Buffer to create packet in
    char packet[1024];
    struct tcphdr *tcp = (struct tcphdr *)packet;
    memset(packet, 0, sizeof(packet));

    // Set the addresses
    int listen_port = RandUtils::randInt(49126, 65000);
    in_addr src_addr, dst_addr;
    src_addr.s_addr = inet_addr(source_address.data());
    dst_addr = ((struct sockaddr_in *)server_info->ai_addr)->sin_addr;
    createTcpHeader(listen_port, atoi(port.data()),
                    TH_SYN | TH_ACK | TH_FIN | TH_RST | TH_URG | TH_PUSH, &src_addr, &dst_addr,
                    tcp);

    // Send packet
    if (sendto(write_socket, &packet, sizeof(struct tcphdr), 0, server_info->ai_addr,
               server_info->ai_addrlen) < 0) {
        perror("Sending message");
        exit(1);
    }

    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(listen_port);

    if (bind(read_socket, (sockaddr *)&serv_addr, server_info->ai_addrlen) < 0) {
        perror("Binding socket");
        exit(1);
    }

    char buffer[256];
    memset(&buffer, 0, sizeof(buffer));

    iphdr *read_iphdr;
    tcphdr *read_tcphdr;
    int recieve_count = 0;
    do {
        recieve_count++;
        int recieved_byte_count = 0;
        if ((recieved_byte_count = recvfrom(read_socket, &buffer, sizeof(buffer), 0,
                                            server_info->ai_addr, &server_info->ai_addrlen)) <
            0) {
            perror("nothing recieved");
            return PortStatus::Closed;
        }

        read_iphdr = (iphdr *)buffer;
        read_tcphdr = (tcphdr *)(buffer + (int)read_iphdr->ihl * 4);
    } while (recieve_count < 10 && (read_tcphdr->th_sport != tcp->th_dport ||
                                    read_tcphdr->th_dport != tcp->th_sport));

    close(read_socket);
    close(write_socket);
    freeaddrinfo(server_info);

    // Port is considered open if no response
    if (read_iphdr->daddr != src_addr.s_addr || read_tcphdr->th_sport != tcp->th_dport ||
        read_tcphdr->th_dport != tcp->th_sport) {
        return PortStatus::Filtered;
    }
    return PortStatus::Closed;
}
