#include "syn_scanner.h"

SynScanner::SynScanner(std::string host, std::string source_address)
    : Scanner(host), source_address(source_address) {}

PortStatus SynScanner::scanPort(std::string port) {
    // Construct addrinfo
    addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_protocol = IPPROTO_TCP;
    addrinfo *server_info = getAddressInfo(port.data(), &hints);
    if (server_info == NULL) {
        return PortStatus::Closed;
    }

    // Setup read/write sockets
    int write_socket = createSocket(PF_INET, SOCK_RAW, IPPROTO_TCP);
    int read_socket = createSocket(PF_INET, SOCK_RAW, IPPROTO_TCP);

    // Set socket timeout
    timeval time_val;
    time_val.tv_sec = 2;
    time_val.tv_usec = 0;
    if (setsockopt(read_socket, SOL_SOCKET, SO_RCVTIMEO, &time_val, sizeof(struct timeval)) <
        0) {
        perror("Setting read socket timeout");
    }
    // Make sure that the socket is cleaned up right away
    int opt = 1;
    if (setsockopt(read_socket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) < 0) {
        perror("Setting read socket REUSEADDR");
    }

    // Buffer to create packet in
    char packet[1024];
    struct tcphdr *tcp = (struct tcphdr *)packet;
    memset(packet, 0, sizeof(packet));

    // Set the addresses and create a tcp header
    int listen_port = RandUtils::randInt(49126, 65000);
    in_addr src_addr, dst_addr;
    src_addr.s_addr = inet_addr(source_address.data());
    dst_addr = ((struct sockaddr_in *)server_info->ai_addr)->sin_addr;
    createTcpHeader(listen_port, atoi(port.data()), TH_SYN, &src_addr, &dst_addr, tcp);

    // Send packet
    if (sendto(write_socket, &packet, sizeof(struct tcphdr), 0, server_info->ai_addr,
               server_info->ai_addrlen) < 0) {
        perror("Sending message");
        exit(1);
    }

    // Bind the read_socket to a specific port
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(listen_port);
    if (bind(read_socket, (sockaddr *)&serv_addr, server_info->ai_addrlen) < 0) {
        perror("Binding socket");
        exit(1);
    }

    // Since recvfrom recieves all messages not just from a specific port or ip address
    // we read the last 10 messages and if none of them match the source and destination port
    // then we assume that we had no answer.
    char buffer[256];
    memset(&buffer, 0, sizeof(buffer));
    iphdr *read_iphdr;
    tcphdr *read_tcphdr;
    int recieve_count = 0;
    do {
        recieve_count++;
        int recieved_byte_count = 0;
        if ((recieved_byte_count = recvfrom(read_socket, &buffer, sizeof(buffer), 0,
                                            server_info->ai_addr, &server_info->ai_addrlen)) <
            0) {
            perror("nothing recieved");
            return PortStatus::Closed;
        }

        read_iphdr = (iphdr *)buffer;
        read_tcphdr = (tcphdr *)(buffer + (int)read_iphdr->ihl * 4);
    } while (recieve_count < 10 && (read_tcphdr->th_sport != tcp->th_dport ||
                                    read_tcphdr->th_dport != tcp->th_sport));

    // Cleanup
    close(read_socket);
    close(write_socket);
    freeaddrinfo(server_info);

    // Parse packet
    if (read_iphdr->daddr != src_addr.s_addr || read_tcphdr->th_sport != tcp->th_dport ||
        read_tcphdr->th_dport != tcp->th_sport) {
        return PortStatus::Closed;
    } else if (read_tcphdr->th_flags == (TH_ACK | TH_SYN)) {
        return PortStatus::Open;
    }
    return PortStatus::Closed;
}
