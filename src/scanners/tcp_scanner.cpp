#include "tcp_scanner.h"

PortStatus TcpScanner::scanPort(std::string port) {
    addrinfo *server_info = getAddressInfo(port.data(), NULL);
    if (server_info == NULL) {
        return PortStatus::Closed;
    }

    // Try opening a socket
    int sockfd =
        socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol);
    if (sockfd < 0) {
        perror("Error opening socket");
        exit(1);
    }

    // Set max connection retries
    int syn_retries = 1;
    setsockopt(sockfd, IPPROTO_TCP, TCP_SYNCNT, &syn_retries, sizeof(int));

    // Try to connect
    bool result = connect(sockfd, server_info->ai_addr, server_info->ai_addrlen) >= 0;

    // Cleanup
    close(sockfd);
    delete server_info;

    return result ? PortStatus::Open : PortStatus::Closed;
}
