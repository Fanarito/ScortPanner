#include "scanner.h"
#include <iostream>

Scanner::Scanner(std::string host) : host(host){};

addrinfo *Scanner::getAddressInfo(const char *port, addrinfo *hints, bool useHost) {
    struct addrinfo *server_info;
    bool cleanup_hints = false;

    if (hints == NULL) {
        hints = new addrinfo;
        // Make sure struct is just zeroes
        memset(hints, 0, sizeof *hints);
        // Set address options
        hints->ai_family = AF_INET;
        hints->ai_socktype = SOCK_STREAM;

        cleanup_hints = true;
    }

    // Get address info
    const char *name = NULL;
    if (useHost)
        name = host.data();
    int res;
    if ((res = getaddrinfo(name, port, hints, &server_info)) != 0) {
        delete server_info;
        server_info = NULL;
    }

    // Cleanup
    if (cleanup_hints)
        delete hints;

    return server_info;
}

sockaddr_in Scanner::getSockAddrIn(const char *port) {
    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(atoi(port));
    sin.sin_addr.s_addr = inet_addr(host.data());
    return sin;
}

int Scanner::createSocket(int domain, int type, int protocol) {
    int read_socket;
    if ((read_socket = socket(domain, type, protocol)) < 0) {
        perror("Creating socket");
        exit(1);
    }
    return read_socket;
}

void Scanner::createTcpHeader(int src_port,
                              int dst_port,
                              int th_flags,
                              in_addr *src_addr,
                              in_addr *dst_addr,
                              tcphdr *tcp) {
    tcp->th_sport = htons(src_port);
    tcp->th_dport = htons(dst_port);
    tcp->th_seq = htonl(random());
    tcp->th_ack = 0;
    tcp->th_x2 = 0;
    tcp->th_off = 5;
    tcp->th_flags = th_flags;
    tcp->th_win = htons(1024);
    tcp->th_sum = 0;
    tcp->th_urp = 0;

    tcp->th_sum = NetUtils::tcp_checksum(src_addr, dst_addr, IPPROTO_TCP, sizeof(tcphdr), tcp);
}
