#include "udp_scanner.h"

PortStatus UdpScanner::scanPort(std::string port) {
    addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    addrinfo *server_info = getAddressInfo(port.data(), &hints);
    if (server_info == NULL) {
        return PortStatus::Closed;
    }

    int write_socket = createSocket(server_info->ai_family, server_info->ai_socktype,
                                    server_info->ai_protocol);

    // Set socket timeout
    timeval time_val;
    time_val.tv_sec = 2;
    time_val.tv_usec = 0;
    if (setsockopt(write_socket, SOL_SOCKET, SO_RCVTIMEO, &time_val, sizeof(timeval)) < 0) {
        perror("Setting read socket timeout");
    }

    // Make sure we recieve IP errors. This lets the ICMP packets through.
    int opt = 1;
    if (setsockopt(write_socket, IPPROTO_IP, IP_RECVERR, &opt, sizeof(int)) < 0) {
        perror("Setting RECVERR");
    }

    // Bind write socket to a random port so that we can recieve messages on that port
    int listen_port = RandUtils::randInt(49126, 65000);
    sockaddr_in sin;
    sin.sin_port = htons(listen_port);
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_family = server_info->ai_family;
    if (bind(write_socket, (sockaddr *)&sin, server_info->ai_addrlen)) {
        perror("binding write socket");
        exit(1);
    }

    char message[128];
    int numbytes;

    // Set source port
    if ((numbytes = sendto(write_socket, &message, sizeof(message), 0, server_info->ai_addr,
                           server_info->ai_addrlen)) == -1) {
        perror("error sending message");
        exit(1);
    }

    // Setup msg header
    // TODO replace with simple recv?
    char buffer[64];
    memset(&buffer, 0, sizeof buffer);
    sockaddr_storage src_addr;
    memset(&src_addr, 0, sizeof src_addr);
    iovec iov[1];
    iov[0].iov_base = buffer;
    iov[0].iov_len = sizeof(buffer);
    int recieved_bytes;
    msghdr msg;
    msg.msg_name = &src_addr;
    msg.msg_namelen = sizeof(src_addr);
    msg.msg_iov = iov;
    msg.msg_iovlen = 1;
    msg.msg_control = 0;
    msg.msg_controllen = 0;

    PortStatus result = PortStatus::Open;

    // Transmit packets twice
    // If the first response is EHOSTUNREACH or ECONNREFUSED then it is closed.
    // If there is no response twice in a row we assume at least one packet got through
    // and that the port is either open or filtered.
    for (int i = 0; i < 2; i++) {
        if ((recieved_bytes = recvmsg(write_socket, &msg, 0)) >= -1) {
            // If the host is unreachable we received an ICMP packet, if it goes past the
            // timeout we assume open/filtered. All other default to closed
            if (errno == EHOSTUNREACH || errno == ECONNREFUSED) {
                result = PortStatus::Closed;
                break;
            } else if (errno == EAGAIN) {
                result = PortStatus::Filtered;
            }
        }
    }

    close(write_socket);
    freeaddrinfo(server_info);

    return result;
}
