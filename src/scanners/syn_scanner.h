#ifndef __SYN_SCANNER_H_
#define __SYN_SCANNER_H_

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/socket.h>

#include "scanner.h"

class SynScanner : public Scanner {
  public:
    SynScanner(std::string host, std::string source_address);

    /**
     * Half open tcp scanning.
     * Sends a SYN packet. If there is a SYN|ACK response the port is open, if there is either
     * no response or SYN|RST port is closed.
     */
    PortStatus scanPort(std::string port);

  private:
    std::string source_address;
};

#endif // __SYN_SCANNER_H_
