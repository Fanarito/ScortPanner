#ifndef __SCANNER_H_
#define __SCANNER_H_

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <string.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "../utils/net_utils.hpp"
#include "../utils/rand_utils.hpp"

enum PortStatus {
    Open,    // Port is open
    Closed,  // Port is closed
    Filtered // Port is either open or filtered
};

class Scanner {
  public:
    explicit Scanner(std::string host);
    virtual ~Scanner(){};

    /**
     * Checks the if the port is open.
     * @returns true if open false otherwise
     */
    virtual PortStatus scanPort(std::string port) = 0;

  protected:
    std::string host;

    /**
     * Gets address info struct for the given port. If hints is null uses defaults.
     * @returns addrinfo if able otherwise NULL
     */
    virtual addrinfo *getAddressInfo(const char *port, addrinfo *hints, bool useHost = true);

    /**
     * Fills in a sockaddr_in struct with the required information
     */
    virtual sockaddr_in getSockAddrIn(const char *port);

    /**
     * Creates socket and handles errors
     */
    virtual int createSocket(int domain, int type, int protocol);

    /**
     * Sets sane defaults for a tcphdr
     */
    virtual void createTcpHeader(int src_port,
                                 int dst_port,
                                 int th_flags,
                                 in_addr *src_addr,
                                 in_addr *dst_addr,
                                 tcphdr *tcp);
};

#endif // __SCANNER_H_
