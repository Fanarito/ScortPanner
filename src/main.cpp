#include <algorithm>
#include <chrono>
#include <future>
#include <iomanip>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <random>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <vector>

#include "profiles.cpp"
#include "scanners/scanner.h"
#include "scanners/syn_scanner.h"
#include "scanners/tcp_scanner.h"
#include "scanners/udp_scanner.h"
#include "scanners/xmas_scanner.h"
#include "utils/net_utils.hpp"
#include "utils/rand_utils.hpp"
#include "utils/string_utils.hpp"

using namespace std;

/**
 * Holds all the different options for scanning
 */
struct ScanOptions {
    string type = "tcp";
    string source_adress;
    int ms_to_wait = 500;
    int rand_extra = 0;
    vector<string> hosts;
    vector<string> ports;
    vector<string> profiles;
};

/**
 * Returns a new scanner object of the type specified
 */
Scanner *getScanner(string host, ScanOptions *options) {
    if (options->type == "tcp")
        return new TcpScanner(host);
    else if (options->type == "syn") {
        if (options->source_adress == "") {
            cout << "syn scantype requires a source address" << endl;
            exit(1);
        }
        return new SynScanner(host, options->source_adress);
    } else if (options->type == "udp") {
        return new UdpScanner(host);
    } else if (options->type == "xmas") {
        if (options->source_adress == "") {
            cout << "xmas scantype requires a source address" << endl;
            exit(1);
        }
        return new XmasScanner(host, options->source_adress);
    } else {
        cout << "Unkown scan type" << endl;
        exit(1);
    }
}

/**
 * Adds all the ports in the profile to the ports collection
 */
void addProfilePorts(string profile, vector<string> *ports) {
    if (profile == "all") {
        for (int i = 0; i <= 65535; i++) {
            ports->push_back(to_string(i));
        }
    } else if (profile == "ics") {
        for (int port : profiles::ICS_PORTS) {
            ports->push_back(to_string(port));
        }
    } else if (profile == "scada") {
        for (int port : profiles::SCADA_PORTS) {
            ports->push_back(to_string(port));
        }
    } else if (profile == "wk_tcp") {
        for (int port : profiles::WK_TCP_PORTS) {
            ports->push_back(to_string(port));
        }
    } else if (profile == "plc") {
        for (int port : profiles::PLC_PORTS) {
            ports->push_back(to_string(port));
        }
    } else {
        cout << "Unkown profile type: " << profile << endl;
        exit(1);
    }
}

/**
 * Scans the ports specified on the host and returns a vector of the open ports.
 */
vector<string> scanHost(string host, ScanOptions options) {
    RandUtils::seedGenerator();
    //  Shuffle ports before scanning
    shuffle(begin(options.ports), end(options.ports), RandUtils::getRng());

    vector<string> open_ports;
    Scanner *scanner = getScanner(host, &options);
    for (string port : options.ports) {
        PortStatus result = scanner->scanPort(port);

        stringstream result_msg;
        result_msg << setw(32) << left << host;
        result_msg << setw(10) << port;
        if (result == PortStatus::Open) {
            open_ports.push_back(port);
            result_msg << setw(6) << "open" << endl;
        } else if (result == PortStatus::Closed) {
            result_msg << setw(6) << "closed" << endl;
        } else if (result == PortStatus::Filtered) {
            result_msg << setw(6) << "open|filtered" << endl;
        }
        cout << result_msg.str();

        // Sleep for the specified time if above 0
        int to_wait = options.ms_to_wait + RandUtils::randInt(0, options.rand_extra);
        if (to_wait > 0)
            this_thread::sleep_for(chrono::milliseconds(to_wait));
    }
    delete scanner;
    return open_ports;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "Usage:" << endl
             << "    spanner [options]" << endl
             << "Options:" << endl
             << "    -t [number]     milliseconds to wait between tests, default 500" << endl
             << "    -r [number]     max random extra milliseconds to wait, default 0" << endl
             << "    -s [scantype]   the type of port scan to use, default tcp" << endl
             << "    -h [hosts]      comma seperated list of hosts" << endl
             << "    -p [ports]      comma seperated list of ports/services" << endl
             << "    -P [profiles]   comma seperated list of profiles" << endl
             << "    -l [ip adress]  the source ip adress TEMPORARY" << endl
             << "Scan Types:" << endl
             << "    tcp" << endl
             << "    syn" << endl
             << "    udp" << endl
             << "    xmas            only works on some bsd's" << endl
             << "Profiles:" << endl
             << "    all             scans all available ports" << endl
             << "    ics             industrial control system ports" << endl
             << "    scada           scada ports" << endl
             << "    plc             known plc ports" << endl
             << "    wk_tcp          well known tcp ports" << endl;
        return 0;
    }

    ScanOptions options;

    // Parse arguments
    for (int i = 1; i < argc; i++) {
        if (strncmp(argv[i], "-t", 2) == 0) {
            i++;
            options.ms_to_wait = stoi(argv[i]);
        } else if (strncmp(argv[i], "-s", 2) == 0) {
            i++;
            options.type = argv[i];
        } else if (strncmp(argv[i], "-h", 2) == 0) {
            i++;
            options.hosts = string_utils::split(argv[i], ',');
        } else if (strncmp(argv[i], "-p", 2) == 0) {
            i++;
            options.ports = string_utils::split(argv[i], ',');
        } else if (strncmp(argv[i], "-P", 2) == 0) {
            i++;
            options.profiles = string_utils::split(argv[i], ',');
            for (string profile : options.profiles) {
                addProfilePorts(profile, &options.ports);
            }
        } else if (strncmp(argv[i], "-r", 2) == 0) {
            i++;
            options.rand_extra = stoi(argv[i]);
        } else if (strncmp(argv[i], "-l", 2) == 0) {
            i++;
            options.source_adress = argv[i];
        }
    }

    // Check if scannable
    if (options.hosts.size() == 0) {
        cerr << "No hosts to scan" << endl;
        exit(1);
    }
    if (options.ports.size() == 0) {
        cerr << "No ports/services to scan" << endl;
        exit(1);
    }

    // Scan hosts
    vector<future<vector<string>>> scanner_threads;
    for (string host : options.hosts) {
        scanner_threads.push_back(async(launch::async, scanHost, host, options));
    }

    for (auto &scanner_thread : scanner_threads) {
        auto r = scanner_thread.get();
    }

    return 0;
}
