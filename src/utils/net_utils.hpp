#ifndef __NET_UTILS_H_
#define __NET_UTILS_H_

#include <cstdint>
#include <ifaddrs.h>
#include <iostream>
#include <netdb.h>
#include <netinet/ip.h>
#include <stddef.h>
#include <sys/types.h>

class NetUtils {
  public:
    /**
     * aldaba-suite
     */
    static int ip_cksum_carry(int x) {
        return (x = (x >> 16) + (x & 0xffff), (~(x + (x >> 16)) & 0xffff));
    }

    /**
     * aldaba-suite
     */
    static int ip_cksum_add(const void *buf, size_t len, int cksum) {
        uint16_t *sp = (uint16_t *)buf;
        int n, sn;

        sn = (int)len / 2;
        n = (sn + 15) / 16;

        /* XXX - unroll loop using Duff's device. */
        switch (sn % 16) {
            case 0:
                do {
                    cksum += *sp++;
                    case 15:
                        cksum += *sp++;
                    case 14:
                        cksum += *sp++;
                    case 13:
                        cksum += *sp++;
                    case 12:
                        cksum += *sp++;
                    case 11:
                        cksum += *sp++;
                    case 10:
                        cksum += *sp++;
                    case 9:
                        cksum += *sp++;
                    case 8:
                        cksum += *sp++;
                    case 7:
                        cksum += *sp++;
                    case 6:
                        cksum += *sp++;
                    case 5:
                        cksum += *sp++;
                    case 4:
                        cksum += *sp++;
                    case 3:
                        cksum += *sp++;
                    case 2:
                        cksum += *sp++;
                    case 1:
                        cksum += *sp++;
                } while (--n > 0);
        }
        if (len & 1)
            cksum += htons(*(u_char *)sp << 8);

        return (cksum);
    }

    /**
     * Thanks to nmap
     */
    static unsigned short tcp_checksum(const struct in_addr *src,
                                       const struct in_addr *dst,
                                       std::uint8_t proto,
                                       std::uint16_t len,
                                       const void *hstart) {
        struct pseudo {
            struct in_addr src;
            struct in_addr dst;
            std::uint8_t zero;
            std::uint8_t proto;
            std::uint16_t length;
        } hdr;
        int sum;

        hdr.src = *src;
        hdr.dst = *dst;
        hdr.zero = 0;
        hdr.proto = proto;
        hdr.length = htons(len);

        /* Get the ones'-complement sum of the pseudo-header. */
        sum = ip_cksum_add(&hdr, sizeof(hdr), 0);
        /* Add it to the sum of the packet. */
        sum = ip_cksum_add(hstart, len, sum);

        /* Fold in the carry, take the complement, and return. */
        sum = ip_cksum_carry(sum);
        /* RFC 768: "If the computed  checksum  is zero,  it is transmitted  as all
         * ones (the equivalent  in one's complement  arithmetic).   An all zero
         * transmitted checksum  value means that the transmitter  generated  no
         * checksum" */
        if (proto == IPPROTO_UDP && sum == 0)
            sum = 0xFFFF;

        return sum;
    }

    static void get_local_addresses() {
        struct ifaddrs *ifaddr, *ifa;
        int family, s, n;
        char host[NI_MAXHOST];

        if (getifaddrs(&ifaddr) == -1) {
            perror("getifaddrs");
        }

        /* Walk through linked list, maintaining head pointer so we
           can free list later */
        for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
            if (ifa->ifa_addr == NULL)
                continue;

            family = ifa->ifa_addr->sa_family;

            /* Display interface name and family (including symbolic
               form of the latter for the common families) */
            printf("%-8s %s (%d)\n", ifa->ifa_name,
                   (family == AF_PACKET)
                       ? "AF_PACKET"
                       : (family == AF_INET) ? "AF_INET"
                                             : (family == AF_INET6) ? "AF_INET6" : "???",
                   family);

            /* For an AF_INET* interface address, display the address */
            if (family == AF_INET || family == AF_INET6) {
                s = getnameinfo(ifa->ifa_addr,
                                (family == AF_INET) ? sizeof(struct sockaddr_in)
                                                    : sizeof(struct sockaddr_in6),
                                host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
                if (s != 0) {
                    printf("getnameinfo() failed: %s\n", gai_strerror(s));
                }

                printf("\t\taddress: <%s>\n", host);
            }
        }

        freeifaddrs(ifaddr);
    }
};

#endif // __NET_UTILS_H_
