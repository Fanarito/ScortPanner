#ifndef __STRING_UTILS_H_
#define __STRING_UTILS_H_

#include <iterator>
#include <sstream>
#include <string>
#include <vector>

namespace string_utils {
    template <typename Out> void split(const std::string &s, char delim, Out result) {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            *(result++) = item;
        }
    }

    std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, std::back_inserter(elems));
        return elems;
    }
} // namespace string_utils

#endif // __STRING_UTILS_H_
