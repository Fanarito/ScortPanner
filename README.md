# ScortPanner

Simple port scanner

## Building

``` sh
mkdir build && cd build
cmake ..
make
```

## Running

``` text
Usage:
    spanner [options]
Options:
    -t [number]     milliseconds to wait between tests, default 500
    -r [number]     max random extra milliseconds to wait, default 0
    -s [scantype]   the type of port scan to use, default tcp
    -h [hosts]      comma seperated list of hosts
    -p [ports]      comma seperated list of ports/services
    -P [profiles]   comma seperated list of profiles
    -l [ip adress]  the source ip adress TEMPORARY
Scan Types:
    tcp
    syn
    udp
    xmas            only works on some bsd's
Profiles:
    all             scans all available ports
    ics             industrial control system ports
    scada           scada ports
    plc             known plc ports
    wk_tcp          well known tcp ports
```

# Influential code

- [nmap](https://github.com/nmap/nmap) checksums and more
- [aldaba-suite](https://github.com/luismartingarcia/aldaba-suite) checksums

# Example output

`./spanner -P plc,ics,scada -h scanme.nmap.org -s syn -r 5000 -l 10.8.8.9`
outputs

``` text
scanme.nmap.org                 63079     closed
scanme.nmap.org                 63082     closed
scanme.nmap.org                 18000     closed
scanme.nmap.org                 10409     closed
scanme.nmap.org                 13724     closed
scanme.nmap.org                 50001     closed
scanme.nmap.org                 8500      closed
scanme.nmap.org                 63027     closed
scanme.nmap.org                 12647     closed
scanme.nmap.org                 10407     closed
scanme.nmap.org                 5065      closed
scanme.nmap.org                 55550     closed
scanme.nmap.org                 12645     closed
scanme.nmap.org                 47808     closed
scanme.nmap.org                 34963     closed
scanme.nmap.org                 9600      closed
scanme.nmap.org                 62963     closed
scanme.nmap.org                 62985     closed
scanme.nmap.org                 12316     closed
scanme.nmap.org                 3004      closed
scanme.nmap.org                 20256     closed
scanme.nmap.org                 38700     closed
scanme.nmap.org                 500       closed
scanme.nmap.org                 10414     closed
scanme.nmap.org                 62992     closed
scanme.nmap.org                 789       closed
scanme.nmap.org                 65443     closed
scanme.nmap.org                 10412     closed
scanme.nmap.org                 4592      closed
scanme.nmap.org                 1090      closed
scanme.nmap.org                 38600     closed
scanme.nmap.org                 63088     closed
scanme.nmap.org                 20257     closed
scanme.nmap.org                 5002      closed
scanme.nmap.org                 1089      closed
scanme.nmap.org                 14592     closed
scanme.nmap.org                 62938     closed
scanme.nmap.org                 44818     closed
scanme.nmap.org                 12289     closed
scanme.nmap.org                 62900     closed
scanme.nmap.org                 34980     closed
scanme.nmap.org                 5050      closed
scanme.nmap.org                 5001      closed
scanme.nmap.org                 62930     closed
scanme.nmap.org                 62924     closed
scanme.nmap.org                 38200     closed
scanme.nmap.org                 38014     closed
scanme.nmap.org                 34964     closed
scanme.nmap.org                 2222      closed
scanme.nmap.org                 2455      closed
scanme.nmap.org                 38589     closed
scanme.nmap.org                 1091      closed
scanme.nmap.org                 47808     closed
scanme.nmap.org                 102       closed
scanme.nmap.org                 502       closed
scanme.nmap.org                 20000     closed
scanme.nmap.org                 12135     closed
scanme.nmap.org                 1025      closed
scanme.nmap.org                 2222      closed
scanme.nmap.org                 38301     closed
scanme.nmap.org                 62911     closed
scanme.nmap.org                 1883      closed
scanme.nmap.org                 38971     closed
scanme.nmap.org                 1153      closed
scanme.nmap.org                 38400     closed
scanme.nmap.org                 38593     closed
scanme.nmap.org                 38210     closed
scanme.nmap.org                 18245     closed
scanme.nmap.org                 502       closed
scanme.nmap.org                 10000     closed
scanme.nmap.org                 1911      closed
scanme.nmap.org                 23        closed
scanme.nmap.org                 63012     closed
scanme.nmap.org                 10449     closed
scanme.nmap.org                 44818     closed
scanme.nmap.org                 1153      closed
scanme.nmap.org                 11001     closed
scanme.nmap.org                 28784     closed
scanme.nmap.org                 48898     closed
scanme.nmap.org                 10447     closed
scanme.nmap.org                 50025     closed
scanme.nmap.org                 9094      closed
scanme.nmap.org                 10431     closed
scanme.nmap.org                 10311     closed
scanme.nmap.org                 57176     closed
scanme.nmap.org                 10428     closed
scanme.nmap.org                 50020     closed
scanme.nmap.org                 2222      closed
scanme.nmap.org                 62981     closed
scanme.nmap.org                 4000      closed
scanme.nmap.org                 2004      closed
scanme.nmap.org                 55000     closed
scanme.nmap.org                 55003     closed
scanme.nmap.org                 102       closed
scanme.nmap.org                 45678     closed
scanme.nmap.org                 13782     closed
scanme.nmap.org                 56001     closed
scanme.nmap.org                 502       closed
scanme.nmap.org                 38011     closed
scanme.nmap.org                 1541      closed
scanme.nmap.org                 5052      closed
scanme.nmap.org                 38000     closed
scanme.nmap.org                 50110     closed
scanme.nmap.org                 44818     closed
scanme.nmap.org                 5094      closed
scanme.nmap.org                 10307     closed
scanme.nmap.org                 8501      closed
scanme.nmap.org                 19999     closed
scanme.nmap.org                 5450      closed
scanme.nmap.org                 63075     closed
scanme.nmap.org                 63094     closed
scanme.nmap.org                 10364     closed
scanme.nmap.org                 20547     closed
scanme.nmap.org                 20000     closed
scanme.nmap.org                 39129     closed
scanme.nmap.org                 102       closed
scanme.nmap.org                 13722     closed
scanme.nmap.org                 5050      closed
scanme.nmap.org                 50018     closed
scanme.nmap.org                 3480      closed
scanme.nmap.org                 39278     closed
scanme.nmap.org                 4999      closed
scanme.nmap.org                 18246     closed
scanme.nmap.org                 62956     closed
scanme.nmap.org                 34962     closed
scanme.nmap.org                 63041     closed
```

`./spanner -p 80,22,443 -h scanme.nmap.org -s syn -r 5000 -l 10.8.8.9`
outputs

``` text
scanme.nmap.org                 443       closed
scanme.nmap.org                 22        open
scanme.nmap.org                 80        open
```
